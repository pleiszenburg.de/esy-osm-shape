esy.osm.shape
=============

An ``esy.osm.shape.Shape`` generates `shapely
<https://shapely.readthedocs.io/>`_ objects from an OpenStreetMap protocol
buffers file (``.pbf``).

``esy.osm.shape.Shape`` objects provide three asynchronous shape generator
methods:

- ``points(filter, chunk_size)``: Generates 
  `points <https://shapely.readthedocs.io/en/latest/manual.html#points>`_
- ``linestrings(filter, chunk_size)``: Generates
  `linestrings
  <https://shapely.readthedocs.io/en/latest/manual.html#linestrings>`_
- ``polygons(filter, chunk_size)``: Generates
  `polygons
  <https://shapely.readthedocs.io/en/latest/manual.html#polygons>`_

The argument *filter* is an optional function to select the OpenStreetMap
primitives for which to generate shapely objects.

Features
--------

What it provides:

- Functionality to generate shapely objects from OpenStreetMap ``.pbf`` data
  file entries.
- Basic plotting tools for matplotlib.

What it **doesn't** provide:

- A mechanism to spatially query OpenStreetMap entries.
- Handle non-geometry objects (like many relations).
- Style information to draw maps.

Installation
------------

``esy.osm.shape`` depends on a Python version of 3.5 or above as well as on
``esy.osm.pbf``. Use ``pip`` to install ``esy.osm.shape``:

.. code:: bash

    $ pip install esy-osm-shape

Example
-------

The following examples operate on a historic dataset for Andorra from 
`geofabrik <https://www.geofabrik.de/>`_. Let's download the dataset first:

>>> import os, urllib.request
>>> if not os.path.exists('andorra.osm.pbf'):
...     filename, headers = urllib.request.urlretrieve(
...         'https://download.geofabrik.de/europe/andorra-190101.osm.pbf',
...         filename='andorra.osm.pbf'
...     )

Open the file and generate linestrings for each *highway* OpenStreetMap entry.

>>> import shapely, esy.osm.shape
>>> shape = esy.osm.shape.Shape('andorra.osm.pbf')
>>> highways = [
...     obj for obj in shape(lambda e: e.tags.get('highway') is not None)
...     if type(obj) is shapely.geometry.LineString
... ]
>>> len(highways)
3948

Using shapely objects it is also easy to compute geometric properties, like for
example the length of the highways (note that the unit of this length is in
longitude and latitude):

>>> sum(linestring.length for linestring in highways)
16.952160743015657

``esy.osm.shape`` also provides functionality to convert shapely objects into
`matplotlib <https://matplotlib.org/>`_ objects.

>>> import matplotlib.pyplot as plt, esy.osm.shape.mpl
>>> fig, ax = plt.subplots(1, 1)
>>> artist = ax.add_artist(
...     esy.osm.shape.mpl.patches(highways, edgecolor='gray', facecolor='none')
... )
>>> xlim = ax.set_xlim(1.38, 1.8)
>>> ylim = ax.set_ylim(42.38, 42.71)
>>> os.makedirs('doc/figures/', exist_ok=True)
>>> fig.savefig('doc/figures/highways.png')

.. _fig-highways:
.. figure:: figures/highways.png

    OpenStreetMap highways in Andorra

All highways in the OSM Andorra dataset are plotted into the figure
fig-highways_.

License
-------

``esy.osm.shape`` is licensed under the `GNU General Public License version
3.0 <https://www.gnu.org/licenses/gpl-3.0.html>`_.

The Team
--------

``esy.osm.shape`` is developed at the
`DLR <https://www.dlr.de/EN/Home/home_node.html>`_ Institute of
`Networked Energy Systems
<https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-12472/21440_read-49440/>`_
in the departement for `Energy Systems Analysis (ESY)
<https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-12471/21741_read-49802/>`_.
