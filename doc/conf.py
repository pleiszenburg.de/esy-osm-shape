project = 'esy-osm-shape'
copyright = '2019, Ontje Lünsdorf'
author = 'Ontje Lünsdorf'

release = '0.1'

extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.autosummary',
]

templates_path = ['_templates']

master_doc = 'contents'

html_theme = 'alabaster'

html_static_path = ['_static']

latex_elements = {
    'classoptions': ',openany,oneside',
    'babel': '\\usepackage[english]{babel}'
}
